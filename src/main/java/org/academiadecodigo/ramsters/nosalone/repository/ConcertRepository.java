package org.academiadecodigo.ramsters.nosalone.repository;


import org.academiadecodigo.ramsters.nosalone.model.Concert;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ConcertRepository extends CrudRepository<Concert, Long> {
}
