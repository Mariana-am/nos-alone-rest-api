package org.academiadecodigo.ramsters.nosalone;

import org.academiadecodigo.ramsters.nosalone.model.Concert;
import org.academiadecodigo.ramsters.nosalone.repository.ConcertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class DemoLoader implements CommandLineRunner {


    private final ConcertRepository concertRepository;

    @Autowired
    public DemoLoader(ConcertRepository concertRepository) {
        this.concertRepository = concertRepository;
    }

    @Override
    public void run(String... strings) throws Exception {

        Calendar concertDate = Calendar.getInstance();
        //concertDate.add(Calendar.DAY_OF_MONTH, 3);
        concertDate.set(2020,05,30,20,30);

        this.concertRepository.save(new Concert(concertDate, "JAIMAO", "ShowDoJaime","Some description", 60, "rock","https://i.picsum.photos/id/705/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "BENNAZI", "My 'Tuna'", "another amazing description", 30,"jazz","https://i.picsum.photos/id/905/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Salvador Sobral", "i'll love by the two of us", "I won the EuroVision thing", 30,"jazz","https://i.picsum.photos/id/79/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Tio Bob", "Roots", "One spliff a day keeps the doctor away", 20,"classic","https://i.picsum.photos/id/91/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Paul Kalkbrenner", "Lets get the party!", "Paul Kalkbrenner  is a German musician, producer of electronic music and actor!", 50, "electronic", "https://i.picsum.photos/id/932/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Smooth Operator", "Lets relax all together", "Pacific ocean vibes", 5,"soul","https://i.picsum.photos/id/914/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Johny Cash", "Hurt", "Food for your soul!", 25, "rock", "https://i.picsum.photos/id/41/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Bob Marley", "Rockpalast", "Truly an experience to be remembered", 80,"reggae","https://i.picsum.photos/id/960/300/200.jpg"));
        this.concertRepository.save(new Concert(concertDate, "Rap && HipHop", "EarthGang", "EarthGang  is an American hip hop duo from Atlanta, Georgia, composed of Atlanta-based rappers",29 ,"hiphop","https://i.picsum.photos/id/987/300/200.jpg"));


    }



}
