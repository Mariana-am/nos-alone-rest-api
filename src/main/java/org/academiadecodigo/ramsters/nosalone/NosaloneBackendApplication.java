package org.academiadecodigo.ramsters.nosalone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NosaloneBackendApplication extends ServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(NosaloneBackendApplication.class, args);
	}

}
